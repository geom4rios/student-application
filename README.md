# How to run the app

## Prerequisities

- Have nodejs & npm install on your system
- Have Python 2.7 or newer (including 3.4+)
- Have EmberJs installed on your system (npm install -g ember-cli)
- Have connection to the internet

## Installation instructions

1. Create a directory i.e STUDENT_APPLICATION on a preferred location on your machine
2. Navigate to the STUDENT_APPLICATION directory  

### Backend setup (Python)

- clone the backend repository (Python) ->  `git clone git@bitbucket.org:geom4rios/student-application.git`
- Install bottle using one of the 3 ways mentioned below

`$ sudo pip install bottle # recommended`

`$ sudo easy_install bottle # alternative without pip`

`$ sudo apt-get install python-bottle # works for debian, ubuntu, ...`

- Navigate to the backend directory (student-application) and run the command -> `python db-setup.py`, this will create the 
<b>studentapp.db</b> file and insert some data into the database.

- After the db is created then run the application using the command -> `python student-application.py` 

### Frontend setup (EmberJs)

- Navigate to teh STUDENT_APPLICATION directory 
- clone the frontend repository (EmberJs) ->  `git clone git@bitbucket.org:geom4rios/student-application-gui.git`
- navigate to the student-application-gui directory and run the command `npm i` 
- Finally run the command `ember serve` and once done from your browser navigate to http://localhost:4200 

