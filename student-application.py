import json

from bottle import Bottle, run, request, response

from service import student_service, grade_information_service

app = Bottle()


@app.hook('after_request')
def enable_cors():
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'POST, PUT, DELETE, GET, OPTIONS'
    response.headers['Access-Control-Request-Method'] = '*'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, X-AUTH-TOKEN, X-API-VERSION, X-CSRF-Token'
    response.headers['Access-Control-Max-Age'] = "1728000"


@app.route('/student', method=['OPTIONS', 'GET'])
def get_students():
    students_from_db = student_service.fetch_students_from_db()
    for student in students_from_db:
        student_id = student['id']
        grades_for_student = grade_information_service.fetch_grades_by_student(student_id)
        student['gradeInformation'] = grades_for_student
    return json.dumps(students_from_db)


@app.route('/student/<student_id:int>', method=['OPTIONS', 'GET'])
def get_student_by_id(student_id):
    student = student_service.fetch_student_by_id(student_id)
    grades = grade_information_service.fetch_grades_by_student(student_id)
    student['gradeInformation'] = grades
    return student


@app.post('/student')
def create_student():
    return student_service.create_student(request.json)


@app.post('/grade', method=['OPTIONS', 'POST'])
def create_grade_for_student():
    if request.method == 'OPTIONS':
        return {"status": 200}
    return student_service.add_grade_to_student(request.json)


@app.route('/available-quarters', method=['OPTIONS', 'GET'])
def get_available_quarters():
    available_quarters = {'yearQuarters': grade_information_service.available_quarters()}
    return json.dumps(available_quarters)


@app.route('/statistics/studentperquarter/<student_id:int>', method=['OPTIONS', 'GET'])
def get_student_average_per_quarter(student_id):
    student = student_service.fetch_student_by_id(student_id)
    grades = grade_information_service.fetch_grades_by_student(student_id)
    student_average = {'id': student['id'], 'username': student['username'], 'name': student['name'], 'quarters': []}
    for grade in grades:
        year = grade['year']
        quarter = grade['quarter']
        year_quarter = str(year) + "-" + str(quarter)
        average = (grade['math'] + grade['computer'] + grade['literature'])/3
        student_average['quarters'].append({"quarter": year_quarter, "average": average})
    return student_average


@app.route('/statistics/yearquarteraverage/<year:int>/<quarter>', method=['OPTIONS', 'GET'])
def get_year_quarter_average(year, quarter):
    return grade_information_service.year_quarter_average(year, quarter)


@app.route('/statistics/subjectperquarter/<subject>', method=['OPTIONS', 'GET'])
def get_subject_average_per_quarter(subject):
    subject_average = grade_information_service.year_quarter_average_per_subject(subject)
    subject_average_response = {'quarters': subject_average, 'subject': subject}
    return json.dumps(subject_average_response)


run(app, host='localhost', port=8080, debug=True, reloader=True)
