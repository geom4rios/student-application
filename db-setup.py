import sqlite3
conn = sqlite3.connect('studentapp.db') # file created in the current directory

# create student db table
conn.execute("CREATE TABLE IF NOT EXISTS student (id INTEGER PRIMARY KEY AUTOINCREMENT,username VARCHAR(255) NOT NULL UNIQUE,name VARCHAR(255) NOT NULL,date_of_birth DATETIME(6) NOT NULL,class VARCHAR(255) NOT NULL)")
# create grade information table
conn.execute("CREATE TABLE IF NOT EXISTS grade_information (id INTEGER PRIMARY KEY AUTOINCREMENT,year INTEGER NOT NULL,quarter VARCHAR(255) NOT NULL,math INTEGER NOT NULL,computer INTEGER NOT NULL,literature INTEGER NOT NULL,student_id INTEGER NOT NULL,FOREIGN KEY (student_id) REFERENCES student(id))")

conn.execute("INSERT INTO student (username, name, date_of_birth, class) VALUES ('Johnny', 'John', DATE('1984-11-10'), 'IT Class')")
conn.execute("INSERT INTO student (username, name, date_of_birth, class) VALUES ('Mary', 'Mary', DATE('1983-12-13'), 'IT Class')")
conn.execute("INSERT INTO student (username, name, date_of_birth, class) VALUES ('Mikey', 'Mike', DATE('1984-07-12'), 'Math Class')")
conn.execute("INSERT INTO student (username, name, date_of_birth, class) VALUES ('Nikos', 'Nicholas', DATE('1984-05-08'), 'Math Class')")
conn.execute("INSERT INTO student (username, name, date_of_birth, class) VALUES ('Tommy', 'Tom', DATE('1984-12-08'), 'Literature Class')")
conn.execute("INSERT INTO student (username, name, date_of_birth, class) VALUES ('Pete', 'Peter', DATE('1983-01-03'), 'Literature Class')")
conn.execute("INSERT INTO student (username, name, date_of_birth, class) VALUES ('Martina', 'Martina', DATE('1984-07-05'), 'Literature Class')")
conn.execute("INSERT INTO student (username, name, date_of_birth, class) VALUES ('Teo', 'Theodore', DATE('1984-01-25'), 'Literature Class')")

# John
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q1', 9, 9, 5, 1)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q2', 6, 10, 4, 1)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q3', 6, 7, 5, 1)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q4', 10, 5, 8, 1)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q1', 9, 4, 5, 1)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q2', 7, 8, 7, 1)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q3', 6, 6, 4, 1)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q4', 4, 7, 5, 1)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q1', 10, 9, 10, 1)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q2', 6, 10, 8, 1)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q3', 10, 8, 7, 1)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q4', 8, 5, 10, 1)")

# Mary
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q1', 9, 9, 10, 2)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q2', 10, 5, 7, 2)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q3', 7, 9, 9, 2)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q4', 6, 8, 4, 2)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q1', 6, 8, 8, 2)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q2', 10, 6, 6, 2)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q3', 9, 4, 6, 2)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q4', 4, 7, 6, 2)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q1', 4, 5, 9, 2)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q2', 6, 8, 8, 2)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q3', 9, 8, 6, 2)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q4', 7, 7, 6, 2)")

# Mike
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q1', 9, 8, 8, 3)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q2', 9, 9, 7, 3)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q3', 6, 4, 10, 3)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q4', 9, 10, 10, 3)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q1', 9, 8, 7, 3)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q2', 10, 7, 6, 3)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q3', 10, 5, 6, 3)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q4', 6, 5, 5, 3)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q1', 7, 4, 6, 3)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q2', 4, 10, 4, 3)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q3', 9, 7, 6, 3)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q4', 6, 9, 9, 3)")

# Nicholas
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q1', 10, 7, 7, 4)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q2', 6, 8, 10, 4)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q3', 4, 9, 10, 4)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q4', 9, 9, 5, 4)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q1', 4, 7, 8, 4)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q2', 5, 7, 6, 4)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q3', 7, 7, 10, 4)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q4', 6, 6, 7, 4)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q1', 4, 7, 9, 4)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q2', 4, 4, 5, 4)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q3', 4, 8, 10, 4)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q4', 10, 4, 10, 4)")

# Tom
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q1', 10, 4, 8, 5)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q2', 7, 4, 7, 5)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q3', 7, 7, 5, 5)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q4', 10, 9, 10, 5)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q1', 9, 7, 7, 5)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q2', 5, 4, 8, 5)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q3', 9, 10, 7, 5)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q4', 6, 6, 10, 5)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q1', 6, 6, 7, 5)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q2', 6, 8, 9, 5)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q3', 10, 9, 4, 5)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q4', 5, 7, 5, 5)")

# Peter
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q1', 5, 6, 9, 6)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q2', 6, 10, 6, 6)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q3', 4, 6, 4, 6)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q4', 10, 4, 5, 6)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q1', 8, 6, 8, 6)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q2', 8, 7, 10, 6)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q3', 10, 9, 8, 6)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q4', 4, 6, 9, 6)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q1', 5, 10, 4, 6)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q2', 4, 7, 4, 6)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q3', 7, 6, 4, 6)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q4', 8, 10, 10, 6)")

# Martina
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q1', 6, 6, 9, 7)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q2', 8, 5, 5, 7)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q3', 10, 7, 7, 7)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q4', 7, 6, 7, 7)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q1', 8, 9, 6, 7)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q2', 5, 5, 10, 7)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q3', 4, 8, 4, 7)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q4', 6, 6, 8, 7)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q1', 4, 8, 7, 7)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q2', 7, 8, 7, 7)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q3', 4, 4, 7, 7)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q4', 7, 8, 9, 7)")

# Teo
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q1', 9, 4, 6, 8)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q2', 5, 5, 10, 8)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q3', 8, 9, 5, 8)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2009', 'Q4', 5, 4, 10, 8)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q1', 7, 4, 5, 8)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q2', 10, 8, 8, 8)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q3', 9, 5, 7, 8)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2010', 'Q4', 8, 10, 8, 8)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q1', 6, 8, 7, 8)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q2', 10, 4, 9, 8)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q3', 6, 4, 6, 8)")
conn.execute("INSERT INTO grade_information(year, quarter, math, computer, literature, student_id) VALUES ('2011', 'Q4', 7, 6, 4, 8)")

conn.commit()
conn.close()
