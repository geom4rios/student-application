import sqlite3
import collections


def fetch_grades_by_student(student_id):
    conn = sqlite3.connect('studentapp.db')
    c = conn.cursor()
    c.execute("SELECT * FROM grade_information WHERE student_id=:student_id", {"student_id": student_id})
    rows = c.fetchall()
    grade_list = []
    for row in rows:
        d = construct_grade_from_db_row(row)
        grade_list.append(d)
    return grade_list


def available_quarters():
    conn = sqlite3.connect('studentapp.db')
    c = conn.cursor()
    c.execute("select year, quarter from grade_information group by year, quarter")
    rows = c.fetchall()
    available_year_quarters = []
    for row in rows:
        year = row[0]
        quarter = row[1]
        year_quarter = str(year) + '-' + str(quarter)
        available_year_quarters.append(year_quarter)
    return available_year_quarters


def year_quarter_average_per_subject(subject):
    conn = sqlite3.connect('studentapp.db')
    c = conn.cursor()
    if (subject.lower() == 'math'):
        c.execute("SELECT year, quarter, AVG(math) as average FROM grade_information group by year, quarter")
    elif (subject.lower() == 'computer'):
        c.execute("SELECT year, quarter, AVG(computer) as average FROM grade_information group by year, quarter")
    elif (subject.lower() == 'literature'):
        c.execute("SELECT year, quarter, AVG(literature) as average FROM grade_information group by year, quarter")
    else:
        return {}

    rows = c.fetchall()
    year_quarter_average = []
    for row in rows:
        d = {}
        year_quarter = str(row[0]) + "-" + str(row[1])
        d['quarter'] = year_quarter
        d['average'] = row[2]
        year_quarter_average.append(d)
    return year_quarter_average


def year_quarter_average(year, quarter):
    conn = sqlite3.connect('studentapp.db')
    c = conn.cursor()
    c.execute("SELECT AVG(math) as math_average, AVG(computer) as computer_avergate, AVG(literature) as literature_average FROM grade_information WHERE year=:year and quarter=:quarter", {"year": year, "quarter": quarter})
    row = c.fetchone()
    d = collections.OrderedDict()
    d['math_average'] = row[0]
    d['computer_average'] = row[1]
    d['literature_average'] = row[2]
    return d


def construct_grade_from_db_row(row):
    d = collections.OrderedDict()
    d['id'] = row[0]
    d['year'] = row[1]
    d['quarter'] = row[2]
    d['math'] = row[3]
    d['computer'] = row[4]
    d['literature'] = row[4]
    return d

