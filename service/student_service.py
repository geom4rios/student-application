import sqlite3
import collections


def fetch_student_by_id(student_id):
    conn = sqlite3.connect('studentapp.db')
    c = conn.cursor()
    c.execute("SELECT * FROM student WHERE id=:student_id", {"student_id": student_id})
    row = c.fetchone()
    return construct_student_from_db_row(row)


def fetch_students_from_db():
    conn = sqlite3.connect('studentapp.db')
    c = conn.cursor()
    c.execute("""
                    SELECT id, username, name, date_of_birth, class
                    FROM student
                    """)
    rows = c.fetchall()

    student_list = []
    for row in rows:
        d = construct_student_from_db_row(row)
        student_list.append(d)

    c.close()
    return student_list


def create_student(student):
    username = student['username']
    name = student['name']
    date_of_birth = student['dateOfBirth']
    clazz = student['class']
    conn = sqlite3.connect('studentapp.db')
    c = conn.cursor()
    c.execute("INSERT INTO student (username, name, date_of_birth, class) VALUES (?,?,?,?)", (username, name, date_of_birth, clazz))
    new_id = c.lastrowid
    student['id'] = new_id
    conn.commit()
    c.close()
    return student


def add_grade_to_student(grade_information):
    student_id = grade_information['studentId']
    conn = sqlite3.connect('studentapp.db')
    c = conn.cursor()
    c.execute("INSERT INTO grade_information (year, quarter, math, computer, literature, student_id) VALUES (?,?,?,?,?,?)",
              (grade_information['year'],
               grade_information['quarter'],
               grade_information['math'],
               grade_information['computer'],
               grade_information['literature'],
               student_id))
    new_id = c.lastrowid
    conn.commit()
    c.close()
    grade_information['id'] = new_id
    return grade_information


def construct_student_from_db_row(row):
    d = collections.OrderedDict()
    d['id'] = row[0]
    d['username'] = row[1]
    d['name'] = row[2]
    d['dateOfBirth'] = row[3]
    d['class'] = row[4]
    return d
